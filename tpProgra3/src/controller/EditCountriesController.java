package controller;

import java.util.ArrayList;

import database.CountryDAO;
import model.Country;
import model.Question;
import view.EditCountriesFrame;
import view.FrameListener;

public class EditCountriesController implements FrameListener{
	
	private EditCountriesFrame view;
	
	public EditCountriesController (EditCountriesFrame viewInstance) {
		view = viewInstance;	
		
		// Register as controller
		view.addListener(this);
		
		prepareTable();
	}

	public void prepareTable() {		
		ArrayList <Country> countries = Country.listOfCountries();
		ArrayList <Question> questions = Question.listOfQuestions();
		
		Object[][] data = new Object [countries.size()][questions.size()+1];
		int xCounter = 0;		
	    for(Country country : countries) {
	    	data[xCounter][0] = country.getName();
	    	int yCounter = 1;
	    	for(Boolean attrib : country.getListOfAttr()) {
	    		data[xCounter][yCounter] = attrib.booleanValue();
	    		yCounter++;
	    	}	    	
	    	xCounter++;
	    }
		
		String [] titles = new String [questions.size()+1];
		titles[0] = "País";
	    for(Question question : questions) {
	    	titles[question.getNumber()+1]= question.getText();
	    }
	    
	    view.addTable(data, titles);
	}
	
	@Override
	public void reportAnswer(String option) {
				
	}

	@Override
	public void reportArrayAnswer(Object[][] answer) {
		
		ArrayList<Country> countries = new ArrayList<Country> ();
		for( int rowIndex=0; rowIndex < answer.length; rowIndex++ ) {
			String name = (String) answer[rowIndex][0];
			ArrayList<Boolean> attributes = new ArrayList<Boolean>();  
			for( int columnIndex=1; columnIndex < answer[0].length; columnIndex++ ) {
				attributes.add(Boolean.valueOf(answer[rowIndex][columnIndex].toString()));
			}
			
			countries.add(new Country(name,attributes));
			
		}
		
		CountryDAO.updateCountryListInfo(countries);

		
	}
	
		
}
