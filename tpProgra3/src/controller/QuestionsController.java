package controller;

import application.Akinator;
import model.Question;
import view.FrameListener;
import view.QuestionsFrame;

public class QuestionsController implements FrameListener{
	
	private Akinator game;
	private QuestionsFrame  view;
	private Question lastQuestionAsked;
	private int progressIndicator;
	
	public QuestionsController (Akinator gameInstance, QuestionsFrame viewInstance) {
		game = gameInstance;
		view = viewInstance;	
		progressIndicator = 0;
		// Register as controller
		view.addListener(this);		
				
		// Set elements texts
		view.setButtonsTexts("Sí", "No", "No sé");
		// Throw first question
		displayQuestion();
	}

	private void displayQuestion() {
		lastQuestionAsked = getQuestion();
		view.setQuestion(lastQuestionAsked.getText());		
	}	

	private Question getQuestion() {
		return game.getNextQuestion();
	}

	public void reportAnswer(String option) {
		
		
		switch (option) {
			case "yes": 
				yes();
				break;
			case "no": 
				no();
				break;
			case "dont know": 
				dontKnow();
				break;			
		}		
	}
	
	private void yes() {
		game.keepTrackOfAnswers("Sí");
		game.removeCountries(lastQuestionAsked.getNumber(), true);
		playNextRound();
	}
	
	private void no() {
		game.keepTrackOfAnswers("No");
		game.removeCountries(lastQuestionAsked.getNumber(), false);
		playNextRound();
	}
	
	private void dontKnow() {		
		game.keepTrackOfAnswers("No sé");
		playNextRound();
	}
	
	private boolean keepAsking() {
		return game.keepAsking();
	}
	
	private void playNextRound() {
		progressIndicator += 15;				
						
		int countriesLeft = game.countriesLeft();
		int questionsLeft = game.questionsLeft();
		
		// First check the countries left according to the user answers
		switch (countriesLeft) {
			case 0:
				view.setProgressBar(100);
				view.gameOver(game.getAnswers(), game.getQuestionAsked());
				break;
			case 1: 
				String country = game.getLastCountryName();
				view.setProgressBar(100);
				String flagFile = "/view/20px-Flag_of_"+country+".svg.png";
				view.gameOverWithGuess(country, flagFile);
				break;
			default:
				view.setProgressBar(progressIndicator);
				if(questionsLeft == 0) {
					view.gameOver(game.getAnswers(), game.getQuestionAsked());
				}else {
					displayQuestion();	
				}				
				break;
		}		
	}

	@Override
	public void reportArrayAnswer(Object[][] answer) {
		// TODO Auto-generated method stub
		
	}
	
	
	
}
