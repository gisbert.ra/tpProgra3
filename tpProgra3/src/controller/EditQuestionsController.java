package controller;

import java.util.ArrayList;

import database.QuestionDAO;
import model.Question;
import view.EditQuestionsFrame;
import view.FrameListener;

public class EditQuestionsController implements FrameListener{
	
	private EditQuestionsFrame view;
	
	public EditQuestionsController (EditQuestionsFrame viewInstance) {
		view = viewInstance;	
		
		// Register as controller
		view.addListener(this);
		
		prepareTable();
	}

	public void prepareTable() {		
		ArrayList <Question> questions = Question.listOfQuestions();
		
		Object[][] data = new Object [questions.size()][2];
		int xCounter = 0;		
	    for(Question question: questions) {
	    	data[xCounter][0] = question.getNumber();
	    	data[xCounter][1] = question.getText();	    		    	
	    	xCounter++;
	    }
		
		String [] titles = new String [2];
		titles[0] = "Id";
		titles[1] = "Texto";
	    
	    view.addTable(data, titles);
	}
	
	@Override
	public void reportAnswer(String option) {
				
	}

	@Override
	public void reportArrayAnswer(Object[][] answer) {
		
		ArrayList<Question> questions= new ArrayList<Question> ();
		for( int rowIndex=0; rowIndex < answer.length; rowIndex++ ) {
			int id = (int) answer[rowIndex][0];
			String text = (String) answer[rowIndex][1]; 
			
			questions.add(new Question(text,id));
			
		}
		
		QuestionDAO.updateQuestionListInfo(questions);

		
	}
	
		
}
