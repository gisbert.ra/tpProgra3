package controller;

import java.util.ArrayList;

import javax.swing.JOptionPane;

import application.Akinator;
import model.Country;
import model.Question;
import view.EditCountriesFrame;
import view.EditQuestionsFrame;
import view.FrameListener;
import view.LandingFrame;
import view.QuestionsFrame;

public class LandingController implements FrameListener{	
	
	private LandingFrame  view;
	
	
	public LandingController (LandingFrame viewInstance) {
		
		view = viewInstance;	
		
		// Register as controller
		view.addListener(this);		
				
		// Set elements texts
		view.setTexts("<html>Bienvenido!\nSeleccione una opción para comenzar</html>","Jugar", "Editar países", "Editar preguntas");
	}

	public void reportAnswer(String option) {
		
		switch (option) {
			case "play": 
				play();
				break;
			case "editCountries": 
				editCountries();
				break;
			case "editQuestions": 
				editQuestions();
				break;
		}		
	}
	
	private void play() {
		JOptionPane.showMessageDialog(view, 
				"Piense en un país de sudamerica, intentaré adivinarlo","Vamos a jugar",JOptionPane.PLAIN_MESSAGE);
		
		// Load the list of questions and countries
		ArrayList <Question> listOfQuestions = Question.listOfQuestions();
		ArrayList <Country> listOfCountries = Country.listOfCountries();
		
		// Set up the Akinator
		Akinator game = new Akinator(listOfCountries, listOfQuestions);
		
		QuestionsFrame qFrame = new QuestionsFrame("Akinator - Jugar");
		QuestionsController qController = new QuestionsController(game, qFrame);
		
		view.setVisible(false);		
	}
	
	private void editCountries() {
		EditCountriesFrame cFrame = new EditCountriesFrame("Akinator - Editar Países");
		EditCountriesController cController = new EditCountriesController(cFrame);
		
		view.setVisible(false);
	}
	
	private void editQuestions() {
		EditQuestionsFrame qeFrame = new EditQuestionsFrame("Akinator - Editar Preguntas");
		EditQuestionsController qeController = new EditQuestionsController(qeFrame);
		
		view.setVisible(false);		
	}
	
	
	@Override
	public void reportArrayAnswer(Object[][] answer) {
		// TODO Auto-generated method stub
		
	}
	
	
	
}
