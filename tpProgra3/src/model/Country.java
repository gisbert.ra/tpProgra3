package model;

import java.io.Serializable;
import java.util.ArrayList;

import database.CountryDAO;

public class Country implements Serializable{	
	
	private static final long serialVersionUID = -8011111874386064773L;
	private String name;
	private ArrayList<Boolean> attributes;	
	
	public Country (String name, ArrayList<Boolean>attr) {
		this.name = name;
		this.attributes = attr;		 		
	}
	
	public String getName() {
		return name;
	}
	
	public Boolean getAttributeByIndex(int index) {
		if (index > attributes.size() || index < 0) {
			throw new IndexOutOfBoundsException("Requested an attribute outside the list");
		}
		return attributes.get(index);
	}
	
	public ArrayList<Boolean> getListOfAttr() {
		return attributes;
	}

	public static ArrayList <Country> listOfCountries() {		
		return CountryDAO.getListOfCountries();
	}	
	
	public void setAttribute(int index, boolean attribute) {
		attributes.set(index, attribute);
	}
	
}
