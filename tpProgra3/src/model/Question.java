package model;

import java.io.Serializable;
import java.util.ArrayList;

import database.QuestionDAO;

public class Question implements Serializable{
	
	private static final long serialVersionUID = -5434550434575078047L;
	private String questionText;
	private int questionNumber;
	
	public Question(String text, int number) {
		questionText = text;
		questionNumber = number;
	}
	
	public String getText() {
		return questionText; 
	}
	
	public int getNumber() {
		return questionNumber;
	}
	
	public static ArrayList<Question> listOfQuestions() {		
		return QuestionDAO.getListOfQuestions();
	}	


}
