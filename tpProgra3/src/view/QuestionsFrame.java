package view;

import java.awt.Color;
import java.awt.FlowLayout;
import java.awt.GridLayout;
import java.awt.event.ActionEvent;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.concurrent.TimeUnit;

import javax.swing.ImageIcon;
import javax.swing.JButton;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JOptionPane;
import javax.swing.JPanel;
import javax.swing.JProgressBar;
import javax.swing.border.EmptyBorder;

import application.Configuration;
import model.Question;


@SuppressWarnings("serial")
public class QuestionsFrame extends AppStandarFrame{
	
	private JPanel contentPane;
	private JButton btnYes, btnNo, btnDontKnow;
	private JLabel lblQuestion;
	private JProgressBar progressBar;
	
	public QuestionsFrame(String name) {
		super(name);		
		fillParams();
		super.initialize();			
		addComponents();	
	}

	@Override
	protected void fillParams() {
		Params = new HashMap<String,Object>();
		Params.put("Maximized",new Boolean(false));
		Params.put("setVisible", new Boolean(true));
		Params.put("setDefaultCloseOperation", JFrame.EXIT_ON_CLOSE);
		Params.put("setResizable", new Boolean(true));
		
		double screenHeight = tlk.getScreenSize().getHeight();
		double screenWidth = tlk.getScreenSize().getWidth();
		
		Params.put("setBounds_x", (int)screenWidth/4);
		Params.put("setBounds_y", 0);
		Params.put("setBounds_width", (int)screenWidth/2);
		Params.put("setBounds_height",(int)screenHeight/2);
	}

	
	@SuppressWarnings({ "rawtypes", "unchecked" })
	protected void addComponents() {
		contentPane = new JPanel();
		contentPane.setBorder(new EmptyBorder(5, 5, 5, 5));
		contentPane.setLayout(new GridLayout(6, 1,1,1));
		contentPane.setBackground(new Color(48,213,200));
		setContentPane(contentPane);
		
		lblQuestion = new JLabel("");
		contentPane.add(lblQuestion);
		
		btnYes = new JButton("");		
		btnYes.setName("btnYes");
		contentPane.add(btnYes);
		
		btnNo = new JButton("");
		btnNo.setName("btnNo");
		contentPane.add(btnNo);
		
		btnDontKnow = new JButton("");
		btnDontKnow.setName("btnDontKnow");
		contentPane.add(btnDontKnow);
				
		progressBar = new JProgressBar(0,100);
		progressBar.setStringPainted(true);
		contentPane.add(progressBar);
		
		//Action listeners
		btnYes.addActionListener(this);
		btnNo.addActionListener(this);
		btnDontKnow.addActionListener(this);
	}
	
	public void setQuestion(String question) {
		lblQuestion.setText(question);
	}
	
	public void setProgressBar(int value) {
		progressBar.setValue(value);
	}
		
	public void setButtonsTexts(String btnY, String btnN, String btnDK ) {
		btnYes.setText(btnY);
		btnNo.setText(btnN);
		btnDontKnow.setText(btnDK);
	}
		
	public void actionPerformed(ActionEvent e) {
		String btnName = ((JButton)e.getSource()).getName();
		
		switch (btnName) {
			case "btnYes": reportAnswer("yes");
				break;
			case "btnNo": reportAnswer("no");
				break;
			case "btnDontKnow": reportAnswer("dont know");
				break;				
		}
	}
	
	public void gameOver(ArrayList <String> answers, ArrayList <Question> questions) {
		contentPaneReset();

		contentPane.add(lblQuestion);
		lblQuestion.setForeground(Color.WHITE);		
		
		
		JLabel lblEmpty= new JLabel("");
		contentPane.add(lblEmpty);
		
		int quantityOfDontKnow = 0;
		for(String answer : answers) {			
			if(answer.equals("No sé")) {
				quantityOfDontKnow++;
			}
		}
		
		if(quantityOfDontKnow/answers.size()>0.5) {
			setQuestion("Mejor elegir otro país del que sepas más..");			
		}else {
			setQuestion("Tus respuestas no me ayudaron a adivinar... no me habrás mentido, no?");
		}
		
		finalGoodBye();
	}
	
	public void gameOverWithGuess(String country, String flag) {
		contentPaneReset();

		contentPane.add(lblQuestion);		
		lblQuestion.setForeground(Color.WHITE);		
		setQuestion("Pensaste en " + country +"?");
		JLabel imgLabel = new JLabel(new ImageIcon(Configuration.rootDir + flag));
		contentPane.add(imgLabel);
		
		finalGoodBye(); 		
	}	
	
	private void contentPaneReset() {
		contentPane.removeAll();
		contentPane.repaint();		
	}
	
	private void finalGoodBye() {
		JOptionPane.showMessageDialog(this, 
				"Gracias por jugar",
				"Fin del juego",
				JOptionPane.PLAIN_MESSAGE);
		System.exit(0);
	}
	
}
