package view;

public interface FrameListener {

		public void reportAnswer(String option);
		public void reportArrayAnswer(Object [][] answer);
}
