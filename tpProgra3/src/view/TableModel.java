package view;

import javax.swing.event.TableModelEvent;
import javax.swing.event.TableModelListener;
import javax.swing.table.AbstractTableModel;

public class TableModel extends AbstractTableModel {

	private Object information[][];
	private String columns [];

	public TableModel(Object [][] data, String cols []) {
		information = data;
		columns = cols;
		addTableModelListener( new TableListener() );		
	}

	@Override
	public int getRowCount() {
		return( information.length );
	}

	@Override
	public int getColumnCount() {
		return( information[0].length ); 
	}

	@Override
	public Object getValueAt(int rowIndex, int columnIndex) {
		return( information[rowIndex][columnIndex] ); 
	}

	public void setValueAt( Object value,int row,int col ) {
		information[row][col] = value;
		fireTableDataChanged();
	}

	public boolean isCellEditable( int row,int col ) { 
		return( true ); 
	}

	public String getColumnName(int c) {
		return columns[c];
	}
	
	public Object [][] getTableValues() {
		return information;
	}
	

	class TableListener implements TableModelListener {
		
		public void tableChanged( TableModelEvent evt ) {
//			for( int rowIndex=0; rowIndex < information.length; rowIndex++ ) {
//				for( int columnIndex=0; columnIndex < information[0].length; columnIndex++ ) {
//					System.out.print( information[rowIndex][columnIndex] + " " );
//				}					
//				System.out.println();
//			}
		}
	}


}
