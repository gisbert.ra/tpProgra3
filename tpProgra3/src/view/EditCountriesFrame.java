package view;

import java.awt.BorderLayout;
import java.awt.Color;
import java.awt.Component;
import java.awt.EventQueue;
import java.awt.FlowLayout;
import java.awt.Graphics;
import java.awt.GridBagConstraints;
import java.awt.GridBagLayout;
import java.awt.GridLayout;
import java.awt.Insets;
import java.awt.Label;
import java.awt.event.ActionEvent;
import java.awt.event.KeyEvent;
import java.util.HashMap;

import javax.swing.DefaultListModel;
import javax.swing.ImageIcon;
import javax.swing.JButton;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JList;
import javax.swing.JPanel;
import javax.swing.JProgressBar;
import javax.swing.JScrollPane;
import javax.swing.border.Border;
import javax.swing.border.EmptyBorder;
import javax.swing.table.DefaultTableModel;
import javax.xml.bind.util.JAXBSource;
import javax.swing.JComboBox;
import javax.swing.JComponent;
import javax.swing.JTabbedPane;
import javax.swing.JTable;
import javax.swing.JTextField;
import javax.swing.JSplitPane;

@SuppressWarnings("serial")
public class EditCountriesFrame extends AppStandarFrame {
	
	private JPanel contentPane;
	private JTable table;
	private JButton btnSave;
	
	public EditCountriesFrame(String name) {
		super(name);		
		fillParams();
		super.initialize();			
		addComponents();	
	}
	
	@Override
	protected void fillParams() {
		Params = new HashMap<String,Object>();
		Params.put("Maximized",new Boolean(true));
		Params.put("setVisible", new Boolean(true));
		Params.put("setDefaultCloseOperation", JFrame.EXIT_ON_CLOSE);
		Params.put("setResizable", new Boolean(true));					
	}

	@Override
	protected void addComponents() {
		contentPane = new JPanel();
		contentPane.setBorder(new EmptyBorder(5, 5, 5, 5));
		setContentPane(contentPane);
		contentPane.setLayout(null);		
		setLayout( new BorderLayout() );
		
		btnSave = new JButton("Guardar");		
	    btnSave.setName("btnSave");
	    btnSave.addActionListener(this);
	    btnSave.setBackground(Color.BLUE);
	    btnSave.setForeground(Color.WHITE);
	    contentPane.add(btnSave, BorderLayout.PAGE_END);
	 }
 
	@Override
	public void actionPerformed(ActionEvent e) {		
		Object [][] toSave = ((TableModel) table.getModel()).getTableValues();
		reportArrayAnswer(toSave);
	}
	
	public void addTable(Object [][] data, String [] columns) {
        table = new JTable (new TableModel(data, columns));
        
        table.setShowHorizontalLines( true );
        table.setRowSelectionAllowed( true );
        table.setColumnSelectionAllowed( true );
	    
        table.setSelectionForeground( Color.white );
        table.setSelectionBackground( Color.red );
	    	    
	    contentPane.add( new JScrollPane(table), BorderLayout.NORTH);
	    
	    
	}
	
}
