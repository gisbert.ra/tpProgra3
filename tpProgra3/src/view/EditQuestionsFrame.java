package view;

import java.awt.BorderLayout;
import java.awt.Color;
import java.awt.event.ActionEvent;
import java.util.HashMap;

import javax.swing.JButton;
import javax.swing.JFrame;
import javax.swing.JPanel;
import javax.swing.JScrollPane;
import javax.swing.JTable;
import javax.swing.border.EmptyBorder;

@SuppressWarnings("serial")
public class EditQuestionsFrame extends AppStandarFrame {
	
	private JPanel contentPane;
	private JTable table;
	private JButton btnSave;
	
	public EditQuestionsFrame(String name) {
		super(name);		
		fillParams();
		super.initialize();			
		addComponents();	
	}

	@Override
	protected void fillParams() {
		Params = new HashMap<String,Object>();
		Params.put("Maximized",new Boolean(true));
		Params.put("setVisible", new Boolean(true));
		Params.put("setDefaultCloseOperation", JFrame.EXIT_ON_CLOSE);
		Params.put("setResizable", new Boolean(false));					
	}


	@Override
	protected void addComponents() {
		contentPane = new JPanel();
		contentPane.setBorder(new EmptyBorder(5, 5, 5, 5));
		setContentPane(contentPane);
		contentPane.setLayout(null);		
		setLayout( new BorderLayout() );
		
		btnSave = new JButton("Guardar");		
	    btnSave.setName("btnSave");
	    btnSave.addActionListener(this);
	    contentPane.add(btnSave, BorderLayout.PAGE_END);
	 }
 
	@Override
	public void actionPerformed(ActionEvent e) {		
		Object [][] toSave = ((TableModel) table.getModel()).getTableValues();
		reportArrayAnswer(toSave);
	}
	
	public void addTable(Object [][] data, String [] columns) {
        table = new JTable (new TableModel(data, columns));
        
        table.setShowHorizontalLines( true );
        table.setRowSelectionAllowed( true );
        table.setColumnSelectionAllowed( true );
	    
        table.setSelectionForeground( Color.white );
        table.setSelectionBackground( Color.red );
	    	    
	    contentPane.add( new JScrollPane(table), BorderLayout.NORTH);
	    
	    
	}
	
}
