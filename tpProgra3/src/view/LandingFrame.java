package view;

import java.awt.Color;
import java.awt.GridBagConstraints;
import java.awt.GridBagLayout;
import java.awt.event.ActionEvent;
import java.util.HashMap;

import javax.swing.ImageIcon;
import javax.swing.JButton;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JPanel;

import application.Configuration;


@SuppressWarnings("serial")
public class LandingFrame extends AppStandarFrame{
	
	private JPanel contentPane;
	private JButton btnPlay, btnEditCountries, btnEditQuestions;
	private JLabel lblWelcome;	
	
	public LandingFrame(String name) {
		super(name);		
		fillParams();
		super.initialize();			
		addComponents();
		
	}
	
	@Override
	protected void fillParams() {
		Params = new HashMap<String,Object>();
		Params.put("Maximized",new Boolean(false));
		Params.put("setVisible", new Boolean(true));
		Params.put("setDefaultCloseOperation", JFrame.EXIT_ON_CLOSE);
		Params.put("setResizable", new Boolean(true));
		
		double screenHeight = tlk.getScreenSize().getHeight(); 
		double screenWidth = tlk.getScreenSize().getWidth();
		double imgHeight = new ImageIcon(Configuration.rootDir + "/view/southamerica.gif").getIconHeight()+30 ; 
		if(imgHeight < screenHeight ) {
			screenHeight = imgHeight ; 
		}
		Params.put("setBounds_x", (int)screenWidth/4);
		Params.put("setBounds_y", 0);
		Params.put("setBounds_width", (int)screenWidth/2);
		Params.put("setBounds_height",(int)screenHeight);
		
	}
	
	protected void addComponents() {
		contentPane = new JPanel();
		contentPane.setLayout(new GridBagLayout());
		contentPane.setBackground(new Color(48,213,200));
		setContentPane(contentPane);
		GridBagConstraints c = new GridBagConstraints();
		c.fill = GridBagConstraints.HORIZONTAL;
		c.weightx = 0.5;		
		
		lblWelcome = new JLabel("");
		c.ipady = 50;
		c.gridx = 0;
		c.gridy = 0;		
		contentPane.add(lblWelcome, c);
		
		btnPlay= new JButton("");		
		btnPlay.setName("btnPlay");
		btnPlay.setBackground(Color.WHITE);		
		c.gridx = 1;
		c.gridy = 0;
		contentPane.add(btnPlay,c);
		
		btnEditCountries = new JButton("");
		btnEditCountries.setName("btnEditCountries");
		btnEditCountries.setBackground(Color.YELLOW);
		c.ipady = 0;
		c.gridx = 0;
		c.gridy = 2;
		contentPane.add(btnEditCountries,c);
		
		btnEditQuestions = new JButton("");
		btnEditQuestions.setName("btnEditQuestions");
		btnEditQuestions.setBackground(Color.YELLOW);
		c.gridx = 3;
		c.gridy = 2;
		contentPane.add(btnEditQuestions,c);	
		
		JLabel imgLabel = new JLabel(new ImageIcon(Configuration.rootDir + "/view/southamerica.gif"));
		c.gridx = 1;
		c.gridy = 2;
		contentPane.add(imgLabel,c);

		//Action listeners
		btnPlay.addActionListener(this);
		btnEditCountries.addActionListener(this);
		btnEditQuestions.addActionListener(this);		
		
	}
		
	public void setTexts(String label, String start, String editCountry, String editQuestion ) {
		lblWelcome.setText(label);
		btnPlay.setText(start);
		btnEditCountries.setText(editCountry);
		btnEditQuestions.setText(editQuestion);
	}	
		
	public void actionPerformed(ActionEvent e) {
		String btnName = ((JButton)e.getSource()).getName();
		
		switch (btnName) {
			case "btnPlay": reportAnswer("play");
				break;
			case "btnEditCountries": reportAnswer("editCountries");
				break;
			case "btnEditQuestions": reportAnswer("editQuestions");
				break;
		}
	}

	
}
