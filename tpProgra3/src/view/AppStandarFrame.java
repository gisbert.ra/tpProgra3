package view;

import java.awt.Image;
import java.awt.Toolkit;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.util.ArrayList;
import java.util.HashMap;

import javax.swing.JFrame;

import application.Configuration;

@SuppressWarnings("serial")
public abstract class AppStandarFrame extends JFrame implements ActionListener{
	
	protected ArrayList<Object> Components;
	protected HashMap<String,Object> Params;
	protected ArrayList<FrameListener> listOfListeners;
	protected Toolkit tlk;
	
	/**
	 * Create the frame.
	 */
	public AppStandarFrame(String name) {
		super(name);		
		
		tlk = Toolkit.getDefaultToolkit();
		Image icon = tlk.getImage(Configuration.rootDir + "/view/icon.png");
		setIconImage(icon);
		
	}
	
	protected void initialize() {
		setVisible((boolean) Params.get("setVisible"));		
		setDefaultCloseOperation((int) Params.get("setDefaultCloseOperation"));
				
		if((boolean)Params.get("Maximized")) {
			setExtendedState(MAXIMIZED_BOTH);
		}else {
			int x = (int) Params.get("setBounds_x");
			int y = (int)Params.get("setBounds_y");
			int width = (int)Params.get("setBounds_width");
			int height = (int)Params.get("setBounds_height");
			
			setBounds(x,y,width,height);			
		}		
		setResizable((boolean) Params.get("setResizable"));
		listOfListeners= new ArrayList<FrameListener>();
	}
	
	protected abstract void fillParams();
	
	protected abstract void addComponents();
	
	public void addListener(FrameListener newListener) {
		listOfListeners.add(newListener); 
	}
	
	public abstract void actionPerformed(ActionEvent e);
	
	protected void reportAnswer(String option) {
		for(FrameListener listener : listOfListeners) {
			listener.reportAnswer(option);
		}
	}
	
	protected void reportArrayAnswer(Object [][] answers) {
		for(FrameListener listener : listOfListeners) {
			listener.reportArrayAnswer(answers);
		}
	}

}
