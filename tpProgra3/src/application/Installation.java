package application;

import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.ObjectInputStream;
import java.io.ObjectOutputStream;
import java.io.Serializable;
import java.util.ArrayList;

import model.Country;
import model.Question;

final class Installation implements Serializable{
	
	
	public static void main(String[] args) throws IOException {
		
		int numberOfCountries = 12;
		int numberOfQuestions = 7;
		int [] config = new int [2];
		
		config[0] = numberOfCountries;
		config[1] = numberOfQuestions;
		
		createCountries(numberOfCountries);
		createQuestions(numberOfQuestions);
		createConfig(config);
		
		
		System.out.println("DB Created");
	}
	
	private static void createCountries(int nbrOf) throws IOException {
		
		ArrayList<Country> countries = new ArrayList<Country>();
		
		String nameAR = "Argentina";
		ArrayList<Boolean> arAttrib = new ArrayList<Boolean>();
		arAttrib.add(true);
		arAttrib.add(true);
		arAttrib.add(true);
		arAttrib.add(false);
		arAttrib.add(true);
		arAttrib.add(false);
		arAttrib.add(true);
		
		String nameBO = "Bolivia";
		ArrayList<Boolean> boAttrib = new ArrayList<Boolean>();
		boAttrib.add(false);
		boAttrib.add(false);
		boAttrib.add(false);
		boAttrib.add(true);
		boAttrib.add(true);
		boAttrib.add(false);
		boAttrib.add(true);
	
		String nameBR = "Brasil";
		ArrayList<Boolean> brAttrib = new ArrayList<Boolean>();
		brAttrib.add(true);
		brAttrib.add(true);
		brAttrib.add(false);
		brAttrib.add(false);
		brAttrib.add(true);
		brAttrib.add(true);
		brAttrib.add(false);

		String nameCH = "Chile";
		ArrayList<Boolean> chAttrib = new ArrayList<Boolean>();
		chAttrib.add(true);
		chAttrib.add(false);
		chAttrib.add(true);
		chAttrib.add(false);
		chAttrib.add(false);
		chAttrib.add(false);
		chAttrib.add(false);
		
		String nameCO = "Colombia";
		ArrayList<Boolean> coAttrib = new ArrayList<Boolean>();
		coAttrib.add(true);
		coAttrib.add(false);
		coAttrib.add(false);
		coAttrib.add(true);
		coAttrib.add(false);
		coAttrib.add(true);
		coAttrib.add(true);

		String nameEC = "Ecuador";
		ArrayList<Boolean> ecAttrib = new ArrayList<Boolean>();
		ecAttrib.add(true);
		ecAttrib.add(false);
		ecAttrib.add(true);
		ecAttrib.add(true);
		ecAttrib.add(false);
		ecAttrib.add(false);
		ecAttrib.add(false);

		String nameGU = "Guyana";
		ArrayList<Boolean> guAttrib = new ArrayList<Boolean>();
		guAttrib.add(true);
		guAttrib.add(false);
		guAttrib.add(false);
		guAttrib.add(false);
		guAttrib.add(false);
		guAttrib.add(true);
		guAttrib.add(true);
		
		String namePA = "Paraguay";
		ArrayList<Boolean> paAttrib = new ArrayList<Boolean>();
		paAttrib.add(false);
		paAttrib.add(false);
		paAttrib.add(false);
		paAttrib.add(false);
		paAttrib.add(false);
		paAttrib.add(false);
		paAttrib.add(true);
		
		String namePE = "Peru";
		ArrayList<Boolean> peAttrib = new ArrayList<Boolean>();
		peAttrib.add(true);
		peAttrib.add(false);
		peAttrib.add(true);
		peAttrib.add(false);
		peAttrib.add(false);
		peAttrib.add(false);
		peAttrib.add(true);
				
		String nameSU = "Surinam";
		ArrayList<Boolean> suAttrib = new ArrayList<Boolean>();
		suAttrib.add(true);
		suAttrib.add(false);
		suAttrib.add(false);
		suAttrib.add(false);
		suAttrib.add(false);
		suAttrib.add(false);
		suAttrib.add(true);
		
		String nameUR = "Uruguay";
		ArrayList<Boolean> urAttrib = new ArrayList<Boolean>();
		urAttrib.add(true);
		urAttrib.add(true);
		urAttrib.add(false);
		urAttrib.add(false);
		urAttrib.add(false);
		urAttrib.add(false);
		urAttrib.add(true);
		
		String nameVE = "Venezuela";
		ArrayList<Boolean> veAttrib = new ArrayList<Boolean>();
		veAttrib.add(true);
		veAttrib.add(false);
		veAttrib.add(true);
		veAttrib.add(true);
		veAttrib.add(false);
		veAttrib.add(false);
		veAttrib.add(true);
	
		countries.add(new Country(nameAR, arAttrib));
		countries.add(new Country(nameBO, boAttrib));
		countries.add(new Country(nameBR, brAttrib));		
		countries.add(new Country(nameCH, chAttrib));
		countries.add(new Country(nameCO, coAttrib));
		countries.add(new Country(nameEC, ecAttrib));
		countries.add(new Country(nameGU, guAttrib));
		countries.add(new Country(namePA, paAttrib));
		countries.add(new Country(namePE, peAttrib));
		countries.add(new Country(nameSU, suAttrib));
		countries.add(new Country(nameUR, urAttrib));
		countries.add(new Country(nameVE, veAttrib));
		
		saveCountries(countries);
	}
	
	private static void createQuestions(int nbrOf) throws IOException {
		ArrayList<Question> questions = new ArrayList<Question>();
		
		questions.add(new Question("Tiene salida al mar?",0));
		questions.add(new Question("Ganó un mundial?",1));
		questions.add(new Question("Su nombre contiene una E?",2));
		questions.add(new Question("Su bandera tiene por lo menos una franja amarilla?",3));
		questions.add(new Question("Limita con Paraguay?",4));
		questions.add(new Question("Limita con Venezuela?",5));
		questions.add(new Question("Limita con Brasil?",6));
		
		saveQuestions(questions);
	}
	
	private static void createConfig(int [] config) throws IOException {
		saveConfiguration(config);
	}
	
	private static void saveConfiguration(int [] config) {
		try {
			FileOutputStream fileOutConfig = new FileOutputStream(Configuration.configDB);
			ObjectOutputStream outputConfig =new ObjectOutputStream(fileOutConfig);
			
			for(int index = 0; index < config.length; index++) {
				outputConfig.writeInt(config[index]);
			}
			
			outputConfig.close();
		
		} catch (IOException i) {
	         i.printStackTrace();
	    }
		
	}

	private static void saveCountries(ArrayList <Country> countries) {
		try {
			FileOutputStream fileOutCountries = new FileOutputStream(Configuration.countriesDB);
			ObjectOutputStream outputCountry = new ObjectOutputStream(fileOutCountries);
			
			outputCountry.writeObject(countries);					
			
			outputCountry.close();
			
		} catch (IOException i) {
	         i.printStackTrace();
	    }
	}
	
	private static void saveQuestions(ArrayList <Question> questions) {
		try {
			FileOutputStream fileOutQuestions = new FileOutputStream(Configuration.questionsDB);
			ObjectOutputStream outputQuestion  =new ObjectOutputStream(fileOutQuestions);
			
			outputQuestion.writeObject(questions);
			
			outputQuestion.close();
			
		} catch (IOException i) {
	         i.printStackTrace();
	    }		
	}

	private static ArrayList<Question>readQuestions() {
		try {						
	        FileInputStream fileInQuestions =new FileInputStream(Configuration.questionsDB);
	        ObjectInputStream inputQuestion =new ObjectInputStream(fileInQuestions);
	        
	        ArrayList<Question> ret  = (ArrayList<Question>) inputQuestion.readObject();
	        
	        inputQuestion.close();
	        
	        return ret;
	        
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		
		return null;
	}
	
	private static ArrayList<Country> readCountries(){
		try {
			FileInputStream fileInCountries=new FileInputStream(Configuration.countriesDB);
	        ObjectInputStream inputCountry =new ObjectInputStream(fileInCountries);
	        
	        ArrayList<Country> ret = (ArrayList<Country>) inputCountry.readObject(); 
	        
	        inputCountry.close();
	        
	        return ret;
	        
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		
		return null;
	}
	
}
