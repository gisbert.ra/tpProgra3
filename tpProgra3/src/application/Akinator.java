package application;

import java.util.ArrayList;

import model.Country;
import model.Question;

public class Akinator {

	private ArrayList <Country> CountryList;
	private ArrayList <Question> QuestionList;
	private ArrayList <Question> QuestionsAsked;
	private ArrayList <String> Answers;
	private boolean keepAsking;
	
	public Akinator (ArrayList <Country> countries, ArrayList <Question> questions) {
		CountryList = countries;
		QuestionList = questions;
		QuestionsAsked = new ArrayList<Question>();
		Answers = new ArrayList<String>();
		keepAsking = true;		
	}
	
	public boolean keepAsking() {
		return keepAsking;
	}
	
	public Question getNextQuestion() {		
		if(QuestionList.size()==1) {
			keepAsking = false;
		}else if(QuestionList.size()==0){
			throw new IllegalArgumentException("No hay más preguntas en la lista");
		}
		
		Question question = bestQuestionToAsk();
		QuestionList.remove(question);

		keepTrackOfQuestionsAsked(question);

		return question;
	}
	
	private Question bestQuestionToAsk() {
		int questionIndex = 0;
		int difference = 0;
		
		if(QuestionList.size()==0) {
			throw new IllegalArgumentException("No existen más preguntas");
		}
		Question returnQuestion = QuestionList.get(0);
		
		for(Question question : QuestionList) {
			questionIndex = question.getNumber();
			int answerTrue = 0; 
			int answerFalse= 0;
			
			
			for(Country country : CountryList) {
				if(country.getAttributeByIndex(questionIndex)) {
					answerTrue++;
				}else {
					answerFalse++;
				}					
			}
			
			int tempDiff = Math.max(answerTrue, answerFalse) - Math.min(answerTrue, answerFalse);
			
			if(difference < tempDiff) {
				returnQuestion = question;
				difference = tempDiff;
			}			
		}		
		return returnQuestion;
		
	}
	
	public void removeCountries(int index, boolean answer) {
		ArrayList<Country> Copy = new ArrayList<Country> ();
		
		Copy.addAll(CountryList);
		
		if (CountryList.size()>0) {
			for(Country country : CountryList) {				
				if(country.getAttributeByIndex(index)!=answer) {
					Copy.remove(country);					
				}
			}
		}else {
			throw new IllegalArgumentException("No existen más países");
		}
		
		CountryList = Copy;
		
	}
	
	public int countriesLeft() {
		return CountryList.size();
	}
	
	public int questionsLeft() {
		return QuestionList.size();
	}
	
	private void keepTrackOfQuestionsAsked(Question asked) {
		QuestionsAsked.add(asked);
	}
	
	public void keepTrackOfAnswers(String answer) {
		Answers.add(answer);
	}
	
	public String getLastCountryName() {
		if(countriesLeft()!=1) {
			throw new IllegalArgumentException("Quedan varios países");
		}else {
			return CountryList.get(0).getName();
		}		
	}
	
	public ArrayList<Country> getCountryList(){
		return CountryList;
	}
	
	public ArrayList <Question> getQuestionAsked(){
		return QuestionsAsked; 
	}
	
	public ArrayList <String> getAnswers(){
		return Answers; 
	}
}
