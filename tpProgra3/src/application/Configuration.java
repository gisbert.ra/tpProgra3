package application;

import database.DBHandler;

public class Configuration {
	
	public static final String rootDir = "/home/ricardo/git/tpProgra3/tpProgra3/src";
	public static final String countriesDB = rootDir +"/database/country";
	public static final String questionsDB = rootDir + "/database/question";
	public static final String configDB = rootDir + "/database/config";

	
	public static int quantityOfCountries() {
		return DBHandler.getInstance().numberOfCountries();		
	}
	
	public static int quantityOfQuestions() {
		return DBHandler.getInstance().numberOfQuestions();
	}
	
}
