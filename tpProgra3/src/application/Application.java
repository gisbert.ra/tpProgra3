package application;

import controller.LandingController;
import view.LandingFrame;

public class Application {

	public static void main(String[] args) {		
		
		// Load the app screen
		LandingFrame main = new LandingFrame("Akinator de Sudamérica");
				
		// Set up the controller
		LandingController control = new LandingController(main);		
				
	}

}
