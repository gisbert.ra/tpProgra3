package database;

import java.util.ArrayList;

import model.Question;

public class QuestionDAO {

	public static ArrayList<Question> getListOfQuestions(){
		return DBHandler.getInstance().getListOfQuestions();		
	}	
		
	public static Question getQuestion(int number) {
		Question question = getQuestionByIndex(number);
		
		if(question == null) {
			throw new IllegalArgumentException("La pregunta no existe en la base de datos");
		}else {
			return question;			  
		}		
	}	
	
	private static Question getQuestionByIndex(int index){
		ArrayList <Question> Questions = getListOfQuestions();		
		return Questions.get(index);
	}
	
	public static void updateQuestionListInfo(ArrayList <Question> list ) {
		DBHandler.getInstance().saveQuestions(list);
	}	

}
