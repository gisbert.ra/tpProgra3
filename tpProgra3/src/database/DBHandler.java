package database;

import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.ObjectInputStream;
import java.io.ObjectOutputStream;
import java.util.ArrayList;

import application.Configuration;
import model.Country;
import model.Question;


public class DBHandler{

		private static DBHandler instance = new DBHandler();
		
		/*
		 * For the sake of simplicity, DBHandler will keep track of sensitive
		 * information inside instance variables. It's kind of ugly but, as we are
		 * not implementing a database, a decision has to be made. 
		 * Several times I changed my mind back and forward, and at this moment I want
		 * to continue with the rest of the code... Maybe later I come back to this. 
		 */
		
		private ArrayList <Country> Countries;
		private ArrayList <Question> Questions;
		
		private int quantityOfCountries;
		private int quantityOfQuestions;

		public static DBHandler getInstance() {
			return instance;
		}
		
		private DBHandler() {        
				loadObjects();		
		}
		
		private void loadObjects() {			
			loadGameConfig();
	        loadCountries();
	        loadQuestions();
		}
		
		private void loadGameConfig() {
			try {
				FileInputStream fileInConfig = new FileInputStream(Configuration.configDB);
				ObjectInputStream inputConfig =new ObjectInputStream(fileInConfig);
				
				/*The first parameter saved in the file indicates the quantity of countries
				* and the second parameter indicates the quantity of questions
				*
				*/				
				quantityOfCountries= (int)inputConfig.readInt();				
				quantityOfQuestions= (int)inputConfig.readInt();
				
				inputConfig.close();
				
			} catch (Exception e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
			
		}	
		
		private void loadCountries() {
			try {
				FileInputStream fileInCountries=new FileInputStream(Configuration.countriesDB);
		        ObjectInputStream inputCountry =new ObjectInputStream(fileInCountries);
		        
		        Countries = new ArrayList<Country>();
		        
		        Countries = (ArrayList<Country>) inputCountry.readObject(); 
		        
		        inputCountry.close();
		        
			} catch (Exception e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
		}
		
		private void loadQuestions() {
			try {						
		        FileInputStream fileInQuestions =new FileInputStream(Configuration.questionsDB);
		        ObjectInputStream inputQuestion =new ObjectInputStream(fileInQuestions);
		        		        
		        Questions = new ArrayList<Question>();
		        
		        Questions = (ArrayList<Question>) inputQuestion.readObject();
		        
		        inputQuestion.close();
		        
			} catch (Exception e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
	        
		}
		
		public ArrayList<Country> getListOfCountries() {
			return Countries;
		}

		public ArrayList<Question> getListOfQuestions() {
			return Questions;
		}

		public int numberOfCountries() {
			return quantityOfCountries;
		}

		public int numberOfQuestions() {
			return quantityOfQuestions;
		}
		
		public void saveCountries(ArrayList <Country> countries) {
			try {
				FileOutputStream fileOutCountries = new FileOutputStream(Configuration.countriesDB);
				ObjectOutputStream outputCountry = new ObjectOutputStream(fileOutCountries);
				
				outputCountry.writeObject(countries);					
				
				outputCountry.close();
				
			} catch (IOException i) {
		         i.printStackTrace();
		    }
		}
		
		public void saveQuestions(ArrayList <Question> questions) {
			try {
				FileOutputStream fileOutQuestions = new FileOutputStream(Configuration.questionsDB);
				ObjectOutputStream outputQuestion  =new ObjectOutputStream(fileOutQuestions);
				
				outputQuestion.writeObject(questions);
				
				outputQuestion.close();
				
			} catch (IOException i) {
		         i.printStackTrace();
		    }		
		}




		
}
