package database;

import java.util.ArrayList;

import model.Country;

public class CountryDAO {
	
	public static ArrayList<Country> getListOfCountries() {
		return DBHandler.getInstance().getListOfCountries();		
	}	
		
	public static Country getCountry(String name) {
		Country country = getCountryByName(name); 
		if(country == null) {
			throw new IllegalArgumentException("El país no existe en la base de datos");
		}else {
			return country;			  
		}		
	}	
	
	private static Country getCountryByName(String name) {
		ArrayList <Country>Countries = getListOfCountries();
		
		for(Country country : Countries) {
			if(country.getName()== name) {
				return country;
			}
		}
		return null;
	}
	
	public static void updateCountryListInfo(ArrayList <Country> list ) {
		DBHandler.getInstance().saveCountries(list);
	}
	
	
}
