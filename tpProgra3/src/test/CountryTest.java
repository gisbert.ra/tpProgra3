package test;

import static org.junit.jupiter.api.Assertions.assertThrows;

import org.junit.jupiter.api.Test;

import application.Configuration;
import database.CountryDAO;
import model.Country;

class CountryTest {

	@Test
	void testgetAttributeByIndex() {
		Country testCountry = CountryDAO.getListOfCountries().get(0);
		assertThrows(IndexOutOfBoundsException.class,()->{
			testCountry.getAttributeByIndex(Configuration.quantityOfQuestions()+2);
		});
	}

}
