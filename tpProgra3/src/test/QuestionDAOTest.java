package test;

import static org.junit.jupiter.api.Assertions.assertFalse;
import static org.junit.jupiter.api.Assertions.assertThrows;

import java.util.ArrayList;

import org.junit.jupiter.api.Test;

import application.Configuration;
import database.QuestionDAO;
import model.Question;

class QuestionDAOTest {

	
	@Test
	void testGetListOfquestions() {
		ArrayList<Question> questions = QuestionDAO.getListOfQuestions();
		assertFalse(questions.isEmpty());	
		assertFalse(questions.size()!=Configuration.quantityOfQuestions());
	}

	@Test
	void testGetUnexistentQuestion() {
		assertThrows(IndexOutOfBoundsException.class,()->{
			QuestionDAO.getQuestion(Configuration.quantityOfQuestions()+1);
		});		
	}
}
