package test;

import static org.junit.jupiter.api.Assertions.assertFalse;
import static org.junit.jupiter.api.Assertions.assertNotNull;
import static org.junit.jupiter.api.Assertions.assertTrue;

import java.util.ArrayList;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

import application.Configuration;
import database.DBHandler;
import model.Country;
import model.Question;

class DBHandlerTest {

	@BeforeEach
	void setUp() throws Exception {
		
	}

	@Test
	void testGetInstance() {
		assertNotNull(DBHandler.getInstance());
	}

	@Test
	void testGetFirstAndLastCountry() {		 
		ArrayList <Country> countries = DBHandler.getInstance().getListOfCountries();
		String zero = new String ("Argentina");
		String five = new String("Ecuador");
		
		assertTrue(countries.get(0).getName().equals(zero));		
		assertTrue(countries.get(5).getName().equals(five) );
	}
	
	@Test
	void testGetFirstAndLastQuestion() {		 
		ArrayList <Question> questions = DBHandler.getInstance().getListOfQuestions();
		
		String zero = new String("Tiene salida al mar?");
		String five = new String("Limita con Venezuela?");
		
		assertTrue(questions.get(0).getText().equals(zero));
		assertTrue(questions.get(0).getNumber()==0);
		
		assertTrue(questions.get(5).getText().equals(five));
		assertTrue(questions.get(5).getNumber()==5);
	}

	@Test
	void testGetListOfCountries() {		 
		ArrayList <Country> countries = DBHandler.getInstance().getListOfCountries(); 
		assertFalse(countries.isEmpty());
	}
	
	@Test
	void testGetListOfQuestions() {
		ArrayList <Question> questions = DBHandler.getInstance().getListOfQuestions(); 
		assertFalse(questions.isEmpty());		
	}	
	
	@Test
	void testNumberOfCountries() {
		assertTrue(DBHandler.getInstance().numberOfCountries() == Configuration.quantityOfCountries());		
	}

	@Test
	void testNumberOfQuestions() {
		assertTrue(DBHandler.getInstance().numberOfQuestions() == Configuration.quantityOfQuestions());
	}
	
	

}
