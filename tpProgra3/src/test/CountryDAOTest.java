package test;

import static org.junit.jupiter.api.Assertions.assertFalse;
import static org.junit.jupiter.api.Assertions.assertThrows;

import java.util.ArrayList;

import org.junit.jupiter.api.Test;

import application.Configuration;
import database.CountryDAO;
import model.Country;

class CountryDAOTest {
	
	@Test
	void testGetListOfCountries() {
		ArrayList<Country> countries = CountryDAO.getListOfCountries();
		assertFalse(countries.isEmpty());	
		assertFalse(countries.size()!=Configuration.quantityOfCountries());
	}

	@Test
	void testGetUnexistentCountry() {
		assertThrows(IllegalArgumentException.class,()->{
			CountryDAO.getCountry("Qatar");
		});		
	}
	
}
