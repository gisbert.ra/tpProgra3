package test;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertThrows;

import java.util.ArrayList;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

import application.Akinator;
import model.Country;
import model.Question;

class AkinatorTest {
	
	Akinator game;
	Akinator gameb;
	
	@BeforeEach
	void setAnInstance() {
		ArrayList <Country> countries = new ArrayList <Country> ();
		ArrayList <Question> questions = new ArrayList <Question> ();
		int nbrOf = 6;
		
		String nameAR = "Argentina";
		ArrayList<Boolean> arAttrib = new ArrayList<Boolean>();
		for(int index = 0; index < nbrOf; index++) {
			arAttrib.add(index%2 == 0);			
		}
		
		String nameBR = "Brasil";
		ArrayList<Boolean> brAttrib = new ArrayList<Boolean>();
		for(int index = 0; index < nbrOf; index++) {
			brAttrib.add(index%2 != 0);			
		}
		
		String nameCH = "Chile";
		ArrayList<Boolean> chAttrib = new ArrayList<Boolean>();
		for(int index = 0; index < nbrOf; index++) {
			chAttrib.add(index%3 == 0);			
		}
		
		String nameBO = "Bolivia";
		ArrayList<Boolean> boAttrib = new ArrayList<Boolean>();
		for(int index = 0; index < nbrOf; index++) {
			boAttrib.add(index%2 == 0);			
		}
		
		String namePY = "Paraguay";
		ArrayList<Boolean> pyAttrib = new ArrayList<Boolean>();
		for(int index = 0; index < nbrOf; index++) {
			pyAttrib.add(index%2 != 0);			
		}
		
		String nameUY = "Uruguay";
		ArrayList<Boolean> uyAttrib = new ArrayList<Boolean>();
		for(int index = 0; index < nbrOf; index++) {
			uyAttrib.add(index%4 == 0);			
		}
		
		countries.add(new Country(nameAR, arAttrib));
		countries.add(new Country(nameBR, brAttrib));
		countries.add(new Country(nameCH, chAttrib));
		countries.add(new Country(nameBO, boAttrib));
		countries.add(new Country(namePY, pyAttrib));
		countries.add(new Country(nameUY, uyAttrib));
		
		questions.add(new Question("Tiene un ", 0));
		questions.add(new Question("No tiene un ", 1));
		questions.add(new Question("Parece un ", 2));
		questions.add(new Question("No parece un ", 3));
		questions.add(new Question("Cerca de ", 4));
		questions.add(new Question("Lejos de ", 5));
		
		game = new Akinator(countries, questions);
		
		ArrayList<Country> country = new ArrayList<Country>();
		ArrayList<Question> question = new ArrayList<Question>();
		gameb = new Akinator(country, question);		
	}
	
	@Test
	void testGetNextQuestion() {		
		assertThrows(IllegalArgumentException.class,()->{
			gameb.getNextQuestion();
		});
	}

	@Test
	void testRemoveCountries() {		
		assertThrows(IllegalArgumentException.class,()->{
			gameb.removeCountries(0, true);
		});		
	}

	@Test
	void testCountriesLeft() {
		assertEquals(game.countriesLeft(),6);
		assertEquals(gameb.countriesLeft(),0);
	}

	@Test
	void testQuestionsLeft() {
		assertEquals(game.questionsLeft(),6);
		assertEquals(gameb.questionsLeft(),0);
	}

	@Test
	void testGetLastCountryName() {
		assertThrows(IllegalArgumentException.class,()->{
			gameb.getLastCountryName();
		});		
	}

	@Test
	void testGetCountryList() {
		assertEquals(game.getCountryList().size(),6);
		assertEquals(gameb.getCountryList().size(),0);
	}

}
